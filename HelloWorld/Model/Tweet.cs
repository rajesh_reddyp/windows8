﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloWorld.Model
{
    public class Tweet
    {
        public string Message { get; set; }
        public string Image { get; set; }
    }
}
